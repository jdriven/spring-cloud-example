package com.example;

import org.slf4j.Logger;
import org.slf4j.LoggerFactory;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.netflix.eureka.EnableEurekaClient;
import org.springframework.scheduling.annotation.EnableScheduling;
import org.springframework.scheduling.annotation.Scheduled;
import org.springframework.web.client.RestTemplate;

@SpringBootApplication
@EnableEurekaClient
@EnableScheduling
public class DemoClientApplication {

	private static final Logger LOGGER = LoggerFactory.getLogger(DemoClientApplication.class);

	public static void main(String[] args) {
		SpringApplication.run(DemoClientApplication.class, args);
	}

	@Autowired
	private RestTemplate restTemplate;

	@Scheduled(fixedRate = 1000)
	public void run() {
		LOGGER.info(restTemplate.getForObject("http://demo-service/time", String.class));
	}
}
