package com.example;

import com.netflix.appinfo.EurekaInstanceConfig;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.time.Instant;

@RestController
@RequestMapping("/time")
public class DemoController {

    @Autowired
    private EurekaInstanceConfig instanceConfig;

    @RequestMapping
    public String getTime() {
        return "Instance " + instanceConfig.getMetadataMap().get("instanceId") + ": it is now " + Instant.now().toString();
    }

}
